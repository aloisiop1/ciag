package br.org.ciag.pst;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.org.ciag.dmp.Usuario;

public class UsuarioPST {

	public boolean updateUsuario(Usuario usuario){
		
		boolean ret = false;
		
		EntityManager em = null;				
		
		try {
			
			em = JPAUtil.getEntityManager();
			
			em.getTransaction().begin();			
			em.merge(usuario);
			em.getTransaction().commit();			
			
			ret = true;
			
		} catch (Exception e) {			
			ret = false;
			e.printStackTrace();
			
			if(em != null && em.getTransaction().isActive()){
				em.getTransaction().rollback();
			}			
		}
		
		finally{			
			if(em !=null && em.isOpen()) em.close();
		}
		
		return ret;
		
	}
	
	
	public boolean deleteUsuario(int id){
		
		EntityManager em = null;
		boolean resp = false;
		
		try {
			
			Usuario usuario = null;		
			
			em  = JPAUtil.getEntityManager();		
			
			usuario = em.find(Usuario.class, id);
						
			if(usuario != null){
				em.getTransaction().begin();
				em.remove(usuario);
				em.getTransaction().commit();
			}
			
			resp  = true;
			
		} catch (Exception e) {			
			e.printStackTrace();
		
			if(em != null && em.getTransaction().isActive()){
				em.getTransaction().rollback();
			}			
		}
		
		finally{
			
			if(em.isOpen())	
				em.close();
		}
		return resp;		
		
	}
	

	public Usuario addUsuario(Usuario usuario){
		
		EntityManager em = null;		
		
		try {

			em =  JPAUtil.getEntityManager();			
			
			em.getTransaction().begin();
			em.persist(usuario);		
			em.getTransaction().commit();
						

		} catch (Exception e) {
		
			e.printStackTrace();
			
			if(em != null && em.getTransaction().isActive()){
				em.getTransaction().rollback();
			}			
		} 
		
		finally{			
			if(em.isOpen())	em.close();
		}

		return usuario;		
		
		
	}
	
	
	public Usuario getUsuario(int id){
		
		Usuario usuario  = null;		
		EntityManager em = null;		
				
		
		try {
			em = JPAUtil.getEntityManager();
			usuario = em.find(Usuario.class, id);
						
		} catch (Exception e) {
			e.printStackTrace();
			if(em != null && em.getTransaction().isActive()){
				em.getTransaction().rollback();
			}			 
		}
		finally{
			if(em !=null && em.isOpen()) em.close();	
		}
		
		return usuario;		
		
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Usuario> getUsuarios(){
		
		EntityManager em = null;
		List<Usuario> usuarios = new ArrayList<>();
		
		try {
			em = JPAUtil.getEntityManager();
			
			Query q = em.createQuery("Select U FROM Usuario U");
			
			usuarios = (List<Usuario>) q.getResultList();
					
			
		} catch (Exception e) {
			e.printStackTrace();
		}			
		
		return usuarios;
		
	}

}
