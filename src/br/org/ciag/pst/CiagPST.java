package br.org.ciag.pst;

import javax.persistence.EntityManager;

import br.org.ciag.ctr.CiagCTR;

public class CiagPST {
	
	private UsuarioPST usuarioPST = null;
	private CiagCTR ctr = null;
	
	public CiagPST(CiagCTR ctr) {
		this.ctr = ctr;	
		
		EntityManager em = JPAUtil.getEntityManager();
		em.close();
		em = null;
	}

	public UsuarioPST getUsuarioPST() {
		
		if(usuarioPST == null){
			usuarioPST = new UsuarioPST();
		}
		return usuarioPST;
	}

	public void setUsuarioPST(UsuarioPST usuarioPST) {
		this.usuarioPST = usuarioPST;
	}

}
