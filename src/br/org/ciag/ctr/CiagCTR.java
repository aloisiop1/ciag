package br.org.ciag.ctr;

import br.org.ciag.dmp.CiagDMP;
import br.org.ciag.pst.CiagPST;

public class CiagCTR {

	private CiagDMP dmp = null;
	private CiagPST pst = null;
	
	public CiagCTR() {	
		
		System.out.println("CRIANDO DMP...");
		dmp = new CiagDMP(this);
		System.out.println("CRIANDO PST...");
		pst = new CiagPST(this);		
	}
		
	public CiagDMP getDMP() {
		if(dmp == null){
			dmp = new CiagDMP(this);
		}
		return dmp;
	}

	public void setDmp(CiagDMP dmp) {
		this.dmp = dmp;
	}

	public CiagPST getPST() {
		return pst;
	}

	public void setPST(CiagPST pst) {
		this.pst = pst;
	}
	
}
