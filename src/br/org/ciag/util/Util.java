package br.org.ciag.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class Util {
	
	public static String encriptar_SHA_256(String senha) 
			throws NoSuchAlgorithmException, UnsupportedEncodingException{
		
		String sha256 = null;
		
		if(senha == null) senha = "";
			
		MessageDigest algorithm = MessageDigest.getInstance("SHA-256");		
		byte messageDigest[] = algorithm.digest(senha.getBytes("UTF-8"));
			 
		StringBuilder hexString = new StringBuilder();
		
		for (byte b : messageDigest) {
		  hexString.append(String.format("%02X", 0xFF & b));
		}
			
		sha256 = hexString.toString();
	
		return sha256;
		
	}
	
	public static String getDateTime(ZoneId zoneId, String formatoData) {
		
		String dataHora = Util.getDateString(Instant.now().toEpochMilli(),	DateTimeFormatter.ofPattern(formatoData), 
		zoneId);
	
		return dataHora;
		
	}
	
	public static String getDateString(long milli, DateTimeFormatter formatter, ZoneId zone) {
	    
		Instant instant = Instant.ofEpochMilli(milli);
        ZonedDateTime zonedDateTime = ZonedDateTime.ofInstant(instant, zone);
        String strDate = zonedDateTime.format(formatter);
        
        return strDate;
    }

	

}
