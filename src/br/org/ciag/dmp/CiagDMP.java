package br.org.ciag.dmp;

import br.org.ciag.ctr.CiagCTR;

public class CiagDMP {
		
	private Usuario usuario = null;
	private CiagCTR ctr = null;
	
	
	public CiagDMP(CiagCTR ctr) {
		this.ctr  = ctr;	
	}
	
	public Usuario getUsuario() {
		
		if (usuario == null){
			usuario = new Usuario();			
		}
		
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	

}
