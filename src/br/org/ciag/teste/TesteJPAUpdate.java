package br.org.ciag.teste;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import br.org.ciag.ctr.CiagCTR;
import br.org.ciag.dmp.Usuario;
import br.org.ciag.pst.UsuarioPST;
import br.org.ciag.util.Util;

public class TesteJPAUpdate {
	
	public static void main(String[] args) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		
		CiagCTR ctr = new CiagCTR();
		
		UsuarioPST usuarioPST = ctr.getPST().getUsuarioPST();
		
		Usuario usuario = usuarioPST.getUsuario(1);
		System.out.println(usuario.getSenha());
		
		usuario.setSenha(Util.encriptar_SHA_256("1qaz2wsx3edc"));
		
		usuarioPST.updateUsuario(usuario);
		
		System.out.println(usuario.getSenha());
		
		
	}

}
