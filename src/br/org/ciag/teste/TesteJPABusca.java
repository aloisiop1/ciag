package br.org.ciag.teste;

import java.util.List;

import br.org.ciag.ctr.CiagCTR;
import br.org.ciag.dmp.Usuario;
import br.org.ciag.pst.UsuarioPST;

public class TesteJPABusca {
	
	public static void main(String[] args) {
		
		CiagCTR ctr = new CiagCTR();
		
		UsuarioPST usuarioPST = ctr.getPST().getUsuarioPST();
		
		Usuario usuario = usuarioPST.getUsuario(1);
		
		System.out.println(usuario.getId() + " " + usuario.getNome() + " " + usuario.getEmail());
		
		
		List<Usuario> usuarios = usuarioPST.getUsuarios();
		
		for (Usuario u : usuarios) {
			System.out.println(u.getId() + " " + u.getNome() + " " + u.getEmail());
			
			
			
		}
		
		
		
		
		
	}

}
