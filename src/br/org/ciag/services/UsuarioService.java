package br.org.ciag.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.org.ciag.dmp.Usuario;
import br.org.ciag.pst.JPAUtil;
import br.org.ciag.services.json.ListaDeUsuariosGet;
import br.org.ciag.services.json.DadosUsuario;
import br.org.ciag.services.json.UsuarioAddResp;
import br.org.ciag.services.json.UsuarioGet;
import br.org.ciag.util.Util;


@Path("/usuarios")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UsuarioService {
		
	private EntityManager em = null;
	private Response response = null;
	private Query query = null;

	
	
	@PUT
	@Path("{id}")
	public Response atualizarUsuario(@PathParam("id") int id, DadosUsuario dados){
		
		try {
			
			em = JPAUtil.getEntityManager();
			Usuario usuario = em.find(Usuario.class, id);
			
			if(usuario != null){
			
				usuario.setNome(dados.getNome());
				usuario.setLogin(dados.getLogin());
				usuario.setEmail(dados.getEmail());
				usuario.setSenha(Util.encriptar_SHA_256(dados.getSenha()));
					
				em.getTransaction().begin();
				em.merge(usuario);
				em.getTransaction().commit();
				
			}
						
			response = Response.status(Response.Status.OK)
					.entity("{\"id\":" + id + "}")
					.type(MediaType.APPLICATION_JSON).build();	
			
						
			
			response.getHeaders().add("Access-Control-Allow-Origin", "*");
			response.getHeaders().add("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT");
		
				
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}
		
		return response;
		
	}
	
	@POST
	public Response adicionarUsuario(DadosUsuario dados){
		
		try {
			
			Usuario usuario = new Usuario();
			
			usuario.setEmail(dados.getEmail());
			usuario.setLogin(dados.getLogin());
			usuario.setNome(dados.getNome());
			usuario.setSenha(Util.encriptar_SHA_256(dados.getSenha()));
						
			em = JPAUtil.getEntityManager();
			em.getTransaction().begin();
			em.persist(usuario);
			em.getTransaction().commit();	
			
			
			UsuarioAddResp resp = new UsuarioAddResp(usuario);
			
			response = Response.status(Response.Status.CREATED)
					.entity(resp.toJson())
					.type(MediaType.APPLICATION_JSON).build();
			
			
			
			response.getHeaders().add("Access-Control-Allow-Origin", "*");
			response.getHeaders().add("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT");
		
				
			
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR); 

		}
		
		
		return response;
		
	}

	
	@DELETE
	@Path("{id}")
	public Response apagarUsuario(@PathParam("id") int id){
		
		try {
			
			em = JPAUtil.getEntityManager();
			
			Usuario usuario = em.find(Usuario.class, id);
			if(usuario != null) {
				em.getTransaction().begin();
				em.remove(usuario);
				em.getTransaction().commit();
			}
		
			
			response = Response.status(Response.Status.OK)
					.entity("{\"id\":" + id + "}")
					.type(MediaType.APPLICATION_JSON).build();					
			
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR); 
		}
		
		response.getHeaders().add("Access-Control-Allow-Origin", "*");
		response.getHeaders().add("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT");
	
			
		
		return response;
		
	}
	
	@GET()
	@Path("{id}")
	public Response getUsuario(@PathParam("id") int id){
		
		UsuarioGet usuarioGet  = null;
		
		try {
			
			em = JPAUtil.getEntityManager();
			
			Usuario usuario = em.find(Usuario.class, id);
		
			if(usuario != null){
				usuarioGet = new UsuarioGet(usuario);
			}else{
				usuarioGet = new UsuarioGet();
			}
			
			response = Response.status(Response.Status.OK)
					.entity(usuarioGet.toJson())
					.type(MediaType.APPLICATION_JSON).build();					
			
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR); 
		}
		
			
		response.getHeaders().add("Access-Control-Allow-Origin", "*");
		response.getHeaders().add("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT");
	
			
		
		return response;
		
	}
	
	@GET()
	public Response getTodosUsuarios(){
	
		ListaDeUsuariosGet lista = null;
		
		try {
			
			em = JPAUtil.getEntityManager();
									
			query = em.createQuery("SELECT U FROM Usuario U");
			@SuppressWarnings("unchecked")
			List <Usuario> usuarios = query.getResultList();
			
			lista = new ListaDeUsuariosGet(usuarios);
						
			response = Response.status(Response.Status.OK)
					.entity(lista.toJson())
					.type(MediaType.APPLICATION_JSON).build();					
			
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR); 
		}
				
		
		response.getHeaders().add("Access-Control-Allow-Origin", "*");
		response.getHeaders().add("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT");
	
			
		
		return response;
		
	}
	
}
