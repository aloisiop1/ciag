package br.org.ciag.services.json;

import br.org.ciag.dmp.Usuario;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class UsuarioGet {
	
	public UsuarioGet() {
	
	}
	
	public UsuarioGet(Usuario usuario) {
		id = usuario.getId();
		nome = usuario.getNome();
		login = usuario.getLogin();
		email = usuario.getEmail();
		senha = usuario.getSenha();
	}
	
	@SerializedName("id")
	private int id;
	
	@SerializedName("nome")	
	private String nome;
	
	@SerializedName("login")
	private String login;
	
	@SerializedName("email")
	private String email;
	
	@SerializedName("senha")
	private String senha;
	
	public String toJson(){
		return new Gson().toJson(this);
	}

}
