package br.org.ciag.services.json;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import br.org.ciag.dmp.Usuario;

public class ListaDeUsuariosGet {
	
	@SerializedName("usuarios")
	List<UsuarioGet> usuarios = new ArrayList<>();
	
	public ListaDeUsuariosGet(List<Usuario> lista) {
		
		for (Usuario usuario : lista) {
			usuarios.add(new UsuarioGet(usuario));
		}
			
	}
	
	

	public String toJson(){
		
		 // ainda n�o descobri como remover o nome do n� inicial do json
		
		 String json = new Gson().toJson(this);
		 String json2 = json.replace("{\"usuarios\":", "");
		 String json3 = json2.substring(0, json2.length()-1);
		 
		 return json3;
		
		
	}
	
	public static void main(String[] args) {
		
		List<Usuario> lista = new ArrayList<Usuario>();
		
		lista.add(new Usuario("user1", "login", "senha", "email"));
		lista.add(new Usuario("user2", "login", "senha", "email"));
		lista.add(new Usuario("user2", "login", "senha", "email"));
		
		System.out.println(new ListaDeUsuariosGet(lista).toJson());
	}
	

}
