package br.org.ciag.services.json;

import br.org.ciag.dmp.Usuario;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class UsuarioAddResp {
	
	@SerializedName("id")
	private int id;
	
	@SerializedName("nome")
	private String nome;
	
	@SerializedName("login")
	private String login;
	
	@SerializedName("email")
	private String email;
	
	public UsuarioAddResp(Usuario usuario) {
	
		this.id = usuario.getId();
		this.nome = usuario.getNome();
		this.login = usuario.getLogin();	
		this.email = usuario.getEmail();
	}
	
	public String toJson(){
		return new Gson().toJson(this);
	}
	

}
