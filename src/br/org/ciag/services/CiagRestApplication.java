package br.org.ciag.services;


import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.ws.rs.core.Application;
import org.glassfish.jersey.jettison.JettisonFeature;
import br.org.ciag.ctr.CiagCTR;



//CLASSE INICIAL DA APLICA��O REST
public class CiagRestApplication extends Application {
		
	public CiagRestApplication() {	
		System.out.println("CIAG -  WEBSERVICE REST INICIADO - " + new java.util.Date().toString());
			

		try {
			
			CiagCTR ciagCTR = new CiagCTR();
				
			
		} catch (NoClassDefFoundError e) {
			
			System.out.println(e.getMessage());
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		
	}
		
	
	@Override
	public Map<String, Object> getProperties() {

		Map<String, Object> properties = new HashMap<>();
		properties.put("jersey.config.server.provider.packages", "br.org.ciag.services");		
		return properties;

	}	
	
	
	@Override
	public Set<Object> getSingletons() {
	
		Set<Object> singletons = new HashSet<>(); 		
		
		singletons.add(new JettisonFeature());
		
		return singletons;
	}


}
