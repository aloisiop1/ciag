package br.org.ciag;

import java.time.ZoneId;
import br.org.ciag.ctr.CiagCTR;
import br.org.ciag.util.Util;

public class Ciag {
	
	private static CiagCTR ctr;

	public static void main(String[] args) {
		
		System.out.println("CIAG REST " + Util.getDateTime(ZoneId.of("America/Sao_Paulo"), "dd/MM/yyyy HH:mm:ss"));
		ctr  = new CiagCTR();
		
	}

}
